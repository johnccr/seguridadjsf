package com.thebudgetstore.security;

import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.security.*;
import java.security.spec.*;
import java.util.Arrays;
import java.util.Base64;

import javax.crypto.*;
import javax.crypto.spec.*;

public class Encryption {

	private static final int ITERATIONS = 10000;
	private static final int KEY_LENGTH = 256;

	
	public static String SHA256(String text) {
		MessageDigest digest;
		try {
			digest = MessageDigest.getInstance("SHA-256");
			byte[] hash = digest.digest(text.getBytes(StandardCharsets.UTF_8));
			return bytesToHex(hash);
		} catch (NoSuchAlgorithmException e) {
			return "error:" + e.getMessage();
		}
	}

	public static String bytesToHex(byte[] hash) {
		StringBuilder hexString = new StringBuilder(2 * hash.length);
		for (int i = 0; i < hash.length; i++) {
			String hex = Integer.toHexString(0xff & hash[i]);
			if (hex.length() == 1) {
				hexString.append('0');
			}
			hexString.append(hex);
		}
		return hexString.toString();
	}

	public static byte[] GetSalt(int num) {
		SecureRandom random = new SecureRandom();
		byte saltbytes[] = new byte[num];
		random.nextBytes(saltbytes);
		return saltbytes;
	}

	public static byte[] hashPass(char[] password, byte[] salt) {
		PBEKeySpec spec = new PBEKeySpec(password, salt, ITERATIONS, KEY_LENGTH);
		Arrays.fill(password, Character.MIN_VALUE);
		try {
			SecretKeyFactory skf = SecretKeyFactory.getInstance("PBKDF2WithHmacSHA256");
			return skf.generateSecret(spec).getEncoded();
		} catch (NoSuchAlgorithmException e) {
			throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
		} catch (InvalidKeySpecException e) {
			throw new AssertionError("Error while hashing a password: " + e.getMessage(), e);
		} finally {
			spec.clearPassword();
		}
	}

	
	
	public static String AES256Encrypt(String text, SecretKey Key, IvParameterSpec IV) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.ENCRYPT_MODE, Key, IV);
			byte[] encryptedMessageInBytes = cipher.doFinal(text.getBytes("UTF-8"));
			return new String(Base64.getEncoder().encode(encryptedMessageInBytes));
		} catch (NoSuchAlgorithmException e) {
			return "error:" + e.getMessage();
		} catch (NoSuchPaddingException e) {
			return "error:" + e.getMessage();
		} catch (BadPaddingException e) {
			return "error:" + e.getMessage();
		} catch (IllegalBlockSizeException e) {
			return "error:" + e.getMessage();
		} catch (UnsupportedEncodingException e) {
			return "error:" + e.getMessage();
		} catch (InvalidKeyException e) {
			return "error:" + e.getMessage();
		} catch (InvalidAlgorithmParameterException e) {
			return "error:" + e.getMessage();
		}
	}

	public static String AES256Decrypt(String text, SecretKey Key, IvParameterSpec IV) {
		try {
			Cipher cipher = Cipher.getInstance("AES/CBC/PKCS5Padding");
			cipher.init(Cipher.DECRYPT_MODE, Key, IV);
			byte[] deencryptedMessageInBytes = cipher.doFinal(Base64.getDecoder().decode(text));
			return new String(deencryptedMessageInBytes);
		} catch (NoSuchAlgorithmException e) {
			return "error:" + e.getMessage();
		} catch (NoSuchPaddingException e) {
			return "error:" + e.getMessage();
		} catch (BadPaddingException e) {
			return "error:" + e.getMessage();
		} catch (IllegalBlockSizeException e) {
			return "error:" + e.getMessage();
		} catch (InvalidKeyException e) {
			return "error:" + e.getMessage();
		} catch (InvalidAlgorithmParameterException e) {
			return "error:" + e.getMessage();
		}
	}

	public static SecretKey generateKey(int n) throws NoSuchAlgorithmException {
		KeyGenerator keyGenerator = KeyGenerator.getInstance("AES");
		keyGenerator.init(n);
		SecretKey key = keyGenerator.generateKey();
		return key;
	}

	public static IvParameterSpec generateIv() {
		byte[] iv = new byte[16];
		new SecureRandom().nextBytes(iv);
		return new IvParameterSpec(iv);
	}
}
