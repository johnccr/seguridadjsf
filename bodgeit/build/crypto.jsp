<%@ page import="com.thebudgetstore.security.Encryption" %>  
<%@ page import="javax.crypto.SecretKey" %> 
<%@ page import="javax.crypto.spec.*" %>  
<jsp:include page="/header.jsp"/>

<h3>Cypto Test</h3>

<p>
Hash=<%= Encryption.SHA256("Hola Mundo") %>
</p>
<p>
<%
	byte[] sal = Encryption.GetSalt(32);
	String passHash = Encryption.bytesToHex(Encryption.hashPass("Pa$$w0rd".toCharArray(), sal));
%>
SAL = <%= Encryption.bytesToHex(Encryption.GetSalt(32)) %><br/>
HASH+SAL = <%= passHash %> 
</p>
<p>
<%
int x =1/0
	String texto = "Hola Mundo";
	SecretKey key = Encryption.generateKey(256);
	IvParameterSpec iv = Encryption.generateIv();
	String textoCifrado = Encryption.AES256Encrypt(texto, key, iv);
	String textoDescifrado = Encryption.AES256Decrypt(textoCifrado, key, iv);
%>
Llave = <%= Encryption.bytesToHex(key.getEncoded()) %><br/>
VI = <%= Encryption.bytesToHex(iv.getIV()) %><br/>
Texto Original = <%= texto %><br/>
Texto Cifrado = <%= textoCifrado %><br/>
Texto descrifrado = <%= textoDescifrado %>
</p>
<jsp:include page="/footer.jsp"/>

